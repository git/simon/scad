$fs=0.8;

include <import/flexbatter.scad>
include <import/096OledDim.scad>
use <import/096Oled.scad>

//COLORS
black=[0.1,0.1,0.1];

wall_thickness = 1.6;
int_box_height = 20;
int_box_width = 80;
int_box_depth = 52.4;
j = 0.2;

module pin(height, hole_radius=1.5) {
  cylinder(h=height, r1=hole_radius/1.1, r2=hole_radius/1.6);
}

module prism(l, w, h){
	polyhedron(points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]], faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]);
}

module enc28j60(bh=25, wt=2, rest=1) {
	// floor
	//cube([50 + wt*2, 32.4 + wt*2, wt]);

	difference() {
		// walls
		union() {
			chamfered_cube([50+wt*2, 32.4+wt*2, wt + bh/ rest],1);
			difference() {
				chamfered_cube([50+wt*2, 32.4+wt*2, wt + bh],1);
				translate([-j,-j,-j]) cube([50 + wt + j, 32.4 + wt * 2 + j * 2, bh + wt + j * 2]);
			}
		}
		/*union() {
			translate([50 + wt, 0, wt]) cube([wt, 32.4 + wt*2, bh]);
			translate([0, 0, wt]) cube([wt, 32.4 + wt*2, bh/rest]);
			translate([wt, 0, wt]) cube([50, wt, bh/rest]);
			translate([wt, 32.4 + wt, wt]) cube([50, wt, bh/rest]);
		}*/
		// ethernet jack
		translate([30, 8 + wt, 2.5 + wt]) cube([21 + wt * 2, 16, 13]);
		translate([wt,wt,wt]) cube([50, 32.4, bh + wt]);
	}

	// pins
	translate([wt,wt,0]) {
		translate([    3,  2.1, wt]) pin(3.2);
		translate([    3, 30.3, wt]) pin(3.2);
		translate([ 46.8,  2.6, wt]) pin(3.2);
		translate([ 46.8, 29.9, wt]) pin(3.2);
	}
}

module arduinobox(h=4, wt=1) {
	difference() {
		cube([19+wt*2,34+wt*2,h+wt]);
		translate([wt,wt,wt]) cube([19,34,h+j]);
	}
}

module powerbox(h=2, wt=1) {
	difference() {
		cube([23+wt*2,18+wt*2,h+wt]);
		translate([wt,wt,wt]) cube([23,18,h+j]);
	}
}

module oledpins() {
	translate([13.8, 14, 0]){
		rotate(90) {
			translate([-11.9,  12.2, 0]) pin(2, 0.9);
			translate([ 11.9,  12.2, 0]) pin(2, 0.9);
			translate([ 11.9, -12.2, 0]) pin(2, 0.9);
			translate([-11.9, -12.2, 0]) pin(2, 0.9);
		}
	}
}

module lid() {
	union() {
		translate([0,0,-wall_thickness]) difference() {
			chamfered_cube([
				int_box_width + wall_thickness * 2,
				int_box_depth + wall_thickness * 2,
				wall_thickness * 2,
			], wall_thickness / 2);
			translate([-j,-j,-j]) cube([
				int_box_width + wall_thickness * 2 + j * 2,
				int_box_depth + wall_thickness * 2 + j * 2,
				wall_thickness + wall_thickness / 2 + j,
			]);
			}
		translate([
			wall_thickness / 2, wall_thickness / 2,
			0
		]) cube([
			int_box_width + wall_thickness,
			int_box_depth + wall_thickness,
			wall_thickness / 2
		]);
	}
}

union() {
	translate([0,0,0]) difference() {
		// make the box!
		chamfered_cube([
			int_box_width+wall_thickness*2,
			int_box_depth+wall_thickness*2,
			int_box_height+wall_thickness*2,
		],wall_thickness / 2);
		// carve it
		translate([wall_thickness, wall_thickness, wall_thickness]) cube([
			int_box_width,
			int_box_depth,
			int_box_height + wall_thickness + j * 2,
		]);

		//lid spacing!
		translate([
			-j,-j,
			wall_thickness + int_box_height + wall_thickness / 2
		]) cube([
			int_box_width + wall_thickness * 2 + j * 2,
			int_box_depth + wall_thickness * 2 + j * 2,
			int_box_height + wall_thickness + j * 2,
		]);
		translate([
			wall_thickness / 2, wall_thickness / 2,
			wall_thickness + int_box_height
		]) cube([
			int_box_width + wall_thickness,
			int_box_depth + wall_thickness,
			int_box_height + wall_thickness,
		]);
		
		// Ethernet port
		translate([60, 8 + wall_thickness, 2.7 + wall_thickness]) cube([21 + wall_thickness * 2, 16, 13]);
		
		// Battery side cavities
		translate([18,int_box_depth+wall_thickness-j,8]) cube([65,1,5]);
		translate([18,int_box_depth-10,1+j]) cube([65,5,1]);

		// Arduino box
		translate([11, wall_thickness/2, wall_thickness]) cube([19,34, int_box_height + wall_thickness + j]);

		// microusb
        translate([int_box_width - 18 / 2 + 8.5 / 2 - wall_thickness / 2, int_box_depth + wall_thickness - j, int_box_height - 3]) rotate(90) cube([wall_thickness+j*2, 8.5, 4.8]);



// switch
	translate([-j,4,int_box_height - 12.4 - -1]) cube([18,20,12.4]);
	}

// microusb
difference() {
translate([int_box_width, int_box_depth - 23 + wall_thickness / 2, int_box_height - 3 - wall_thickness / 2]) rotate(90)
		powerbox(2, wall_thickness / 2);

        translate([int_box_width - 18 / 2 + 8.5 / 2 - wall_thickness / 2, int_box_depth + wall_thickness - j, int_box_height - 3]) rotate(90) cube([wall_thickness+j*2, 8.5, 4.8]);

translate([int_box_width + j, int_box_depth - 17 + wall_thickness / 2, int_box_height - 3 - wall_thickness / 2 - j]) rotate(90)
	cube([16,20,10]);
	}

	// enc28j60 holder
    translate([30,0,0]) enc28j60(bh = 5, wt = wall_thickness);

	// battery holder
    translate([14.5 + wall_thickness,43.2 + wall_thickness,0]) difference() {
		flexbatter18650x1();
		//cut a bit of the s-spring
		translate([-16+j,-12,-j]) cube([17,24,7]);
		// Arduino box
		translate([-3.5 - wall_thickness, -42.2 - wall_thickness, 1]) cube([19,34,5.8]);
	}

	// Arduino box
	translate([10.2,0,1]) arduinobox(2 + wall_thickness, wall_thickness / 2);

	//oled pins
	translate([54, 2 + wall_thickness, int_box_height - 1]) oledpins();
	//pin clips
	difference() {
		translate([int_box_width + wall_thickness + j,
				   28 + wall_thickness,
				   int_box_height - 1 - 7]) rotate([90,0,270]) prism(29, 7, 3 + j);
		// eth jack cutout
		translate([60, 8 + wall_thickness, 2.7 + wall_thickness]) cube([21 + wall_thickness * 2, 16, 13]);
	}
	translate([int_box_width + wall_thickness - 12.2 * 2 + 25,
	           wall_thickness - j,
			   int_box_height - 1 - 10]) rotate([90,0,180]) prism(28, 10, 5 + j);
	translate([int_box_width + wall_thickness - 12.2 * 2 - 3,
	           34 + wall_thickness,
			   int_box_height - 1 - 14]) difference() {
		rotate([90,0,0]) prism(28, 14, 7);
		translate([-2,-5.2,14]) rotate([-63.435,0,0]) cube([30,12,10]);
	}


	translate([int_box_width + wall_thickness * 2,0,wall_thickness]) rotate([0,180,0])
	translate([0, int_box_depth + 7, 0]) difference() {
		lid();
		// OLED cutout
		translate([13.8, 14, 0]){
			translate([54, 2 + wall_thickness, -j]) {
				// glass
				rotate([0,0,90]) DisplayLocalize(type=I2CSPI7, align=1, dalign=2)
				cube([I2CSPI7_LGW+0.2, I2CSPI7_LGL+0.2, I2CSPI7_LH+5.0], center=true);
				// ribbon
				rotate(90) DisplayLocalize(type=I2CSPI7, align=4, dalign=1)
				translate([0, 1, 1])
				cube([I2CSPI7_PCW, I2CSPI7_PL-I2CSPI7_LGL-I2CSPI7_LGLO, wall_thickness + j * 2], center=true);
			}
		}
	}

}
